import os
"""Esta funcion nos permite identificar
   que accion se esta realizando y nos-
   otorga el resultado"""

def verificadora():
    if opcion == 2:
        if proteina in wor:
            print(wor.get(proteina))
        else:
            print("Proteina no encontrada\n")
    elif opcion == 3:
        if modificacion in wor:
            wor[modificacion] = str(input("Ingrese la informacion que quiere modificar:   "))
        else:
            print("proteina no encontrada\n")
    elif opcion == 4:
        if nombre in wor:
            print("Ya existe esta proteiona en el repositorio\n")
        else:
            wor[nombre] = str(input("Escriba la informacion que quiera añadir a la proteina:  "))
    elif opcion == 5:
        if eliminar in wor:
            del wor[eliminar]
        else:
            print("Proteina no encontrada\n")
    else:
        Print("El valor ingresado no es valido\n")
 
#Diccionario
"""Contenido inicial del diccionario. El diccionario
   esta ordenado con saltos de línea para poder ide-
   -ntificar con mayor facilidad cada key con su re-
   -spectivo value."""

wor = {"7BQL":"The crystal structure of PdxI complex with the Alder-ene adduct",
       "7BQJ":"The structure of PdxI",
       "7BVZ":"Crystal structure of MreB5 of Spiroplasma citri bound to ADP"}

respuesta = "s"
cont = 0

print("\tBienvenido!\n")

while respuesta != "n" and respuesta != "N":
    print("Que acción desea realizar?\n\nPresione:\n1 para imprimir el diccionario actual\n"
          "2 para buscar información acerca de la proteina\n3 para editar la información de una proteina\n"
          "4 para agregar una nueva proteina\n5 para eliminar la información de una proteina\n")

    opcion = int(input())
    print("")
    if opcion == 1:
        print(wor.keys())
    elif opcion == 2:
        proteina = str(input("¿Que proteina desea buscar?:  "))
        verificadora()
    elif opcion == 3:
        modificacion = str(input("¿La informacion de que proteina desea cambiar?:  "))
        verificadora()
    elif opcion == 4:
        nombre = str(input("Ingrese el nombre de la proteina: "))
        verificadora()
    elif opcion == 5:
        eliminar = str(input("¿La informacion de que proteina desea eliminar?:  "))
        verificadora()
    else:
        print("El valor ingresado no es valido")
    respuesta = str(input("¿Desea continuar?\n\nPresione 's' si desea continuar\nPresione 'n' si no desea continuar\n\nOpcion: "))
    os.system("clear")

