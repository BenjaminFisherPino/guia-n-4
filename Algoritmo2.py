import os

#Contenido inicial del deiccionario
wor = {"Hola":"Hello","hola":"hello","Yo":"I","yo":"I","Mi":"My","mi":"my",
       "Nombre":"Name","nombre":"name","Es":"Is","es":"is","Gusta":"I like",
       "gusta":"I like","Futbol":"Football","futbol":"football","Estudio":"I study",
       "estudio":"I study"}

#Variable respuesta iniciada en "s" para poder entrar al ciclo
respuesta = "s"

#Ciclo while principal
while respuesta != "n" and respuesta != "N":
    os.system("clear)
    opcion = int(input("\nPresione:\n 1 para poder ver el diccionario actual\n 2 para ingresar una palabra "
                       "en español con su traduccion\n 3 para ingresar una oracion y traducirla\n\nOpcion:  "))
    #While que sirve para verificar la opcion ingresada
    while opcion != 1 and opcion != 2 and opcion != 3:
        opcion = int(input("La opcion ingresada no es valida, por favor verifique su respuesta"))
    #Opcion numero 1
    if opcion == 1:
        print(wor.keys())
    #Opcion numero 2
    elif opcion == 2:
        nombre = str(input("Ingrese la palabra en español:  "))
        nombre = nombre.split(sep=';')
        if nombre[0] in wor:
            print("Ya existe esta palabra en el diccionario.\n")
        else:
            wor.update({nombre[0]: nombre[1]})      
    #Opcion numero 3
    elif opcion == 3:
        text = str(input("Ingrece el texto a traducir: ")).split()
        print("\n")
        for i in text:
            if i in wor:
                print(wor[i],end=' ')
            else:
                print(i,end=' ')
    respuesta = str(input("\n\n¿Desea continuar utilizando el programa?\nPresione:\n"
                          "s para continuar\n n para salir del programa\n\nOpcion:  "))
    #Verificador de respuesta
    while respuesta != "s" and respuesta != "S" and respuesta != "n" and respuesta != "N":
        print("\n\n¿Desea continuar utilizando el programa?\nPresione:\n"
              "s para continuar\n n para salir del programa\n\nOpcion:  ")
        respuesta = str(input("La opcion ingresada no es valida, por favor verifique su respuesta:  "))
print("Hasta luego!")
